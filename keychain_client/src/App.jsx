import React, { useState, useEffect } from "react";
import { ethers } from "ethers"; //import ethers library
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Layout from "./component/Layout";
import Public from "./component/Public";

function App() {
  const [provider, setProvider] = useState(null)
  const [account, setAccount] = useState("Not connected");

  useEffect(() => {
    const template = async () => {
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      setProvider(provider)
      
      window.ethereum.on('accountsChanged', async () => {
        const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
        const account = ethers.utils.getAddress(accounts[0])
        setAccount(account);
      })      
    };
    template();
  }, []);

  return (
    <>
    <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Public />} />
        </Route>
    </Routes>
    </>
  );
}

export default App;