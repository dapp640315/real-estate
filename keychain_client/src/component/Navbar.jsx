import React, { useState } from 'react';
import logo from "../../public/logo.png";
import metamask from "../../src/assets/metamask-logo.png";
import { Link } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
import { ethers } from 'ethers';

const Navbar = () => {
  const location = useLocation();

  return (
    <div className='px-16 py-8 flex w-full justify-between items-center bg-[white] fixed backdrop-blur-sm shadow-md'>
      {/* Left side: Logo and navigation links */}
      <div className='flex items-center'>
        <Link to={"/"} className='flex items-center text-bold'>
          <img src={logo} alt="" className='mr-4' />
          <h1 className='font-bold text-2xl'>Keychain</h1>
        </Link>
        <div className='flex gap-20 text-lg ml-16'> 
          <Link to={"/"} onClick={() => handleScrollTo('search-details')} className=''>Search Details</Link>
          <Link to={"/"} onClick={() => handleScrollTo('AboutUs')} className=''>About us </Link>
          <Link to={"/"} onClick={() => handleScrollTo('aim')} className=''>Services</Link>
        </div>
      </div>

      {/* Right side: Connect Wallet button */}
      <div className='pr-10'>
      {account ? (
                <button
                className='bg-[#7065F0] text-white font-medium py-2 px-4 rounded-lg'
                type ="button"
                onClick={connectHandler}> 
                  {account.slice(0, 6) + '...' + account.slice(38, 42)} 
                </button>
             
            ) : (
              <button className='bg-[#7065F0] text-white font-medium py-2 px-4 rounded-lg'
              type ="button"
              onClick={connectHandler}> 
                <img src={metamask} alt="Metamask Logo" className='inline mr-2 w-6 h-6' /> 
                Connect Wallet
              </button>
           
            )}
      </div>
    </div>
  );
};

export default Navbar;