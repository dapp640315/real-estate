import React, { useState, useRef, useEffect } from "react";
import { Link } from "react-router-dom";
import logo from "../../public/logo.png";
import house from "../../src/assets/house.png";
import hd from "../../src/assets/hd.png";
import icon1 from "../../src/assets/Icon1.png";
import icon2 from "../../src/assets/Icon2.png";
import Search from "../../src/assets/searchB.png";
import house1 from "../../src/assets/houseimg/house1.png";
import illustration from "../../src/assets/Illustration.png";
// icons
import ether from "../../src/assets/icons/eth.png";
import bed from "../../src/assets/icons/bed.png";
import bath from "../../src/assets/icons/bathroom.png";
import land from "../../src/assets/icons/land.png";
import phone from "../../src/assets/icons/phone.png";
import location from "../../src/assets/icons/location.png";
import security from "../../src/assets/icons/security.png";
import transparency from "../../src/assets/icons/Transparency.png";
import efficiency from "../../src/assets/icons/efficiency.png";
import Hcheck from "../../src/assets/icons/housecheck.png";
import dollar from "../../src/assets/icons/dollar.png";
import down from "../../src/assets/icons/down.png";
import loco from "../../src/assets/icons/loco.png";

const Public = () => {
  const [searchTerm, setSearchTerm] = React.useState(""); // State for search term
  const [isBuy, setIsBuy] = useState(true); // State to track buy/sell toggle

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };
  // Function to handle card click and toggle expansion
  const [expandedCardIndex, setExpandedCardIndex] = useState(null);
  const handleCardClick = (index) => {
    setExpandedCardIndex(index === expandedCardIndex ? null : index);
  };

  return (
    <div className="main-content">
      <div className="banner flex justify-around items-center bg-gradient-to-b from-[#E0DEF7] to-white">
        <div className="text-content w-1/2 px-40 pt-40 pb-20">
          <div className=" ">
            <h1 className="text-8xl font-semibold">
              Buy and sell your property easily with Keychain
            </h1>
            <h3 className="text-2xl py-12 font-medium">
              A great platform to buy and sell your properties without any
              intermediaries.
            </h3>
          </div>
          <div className="flex justify-between">
            <div>
              <img src={icon1} alt="icon1" />
              <h1 className="text-2xl font-bold">10k+ Sellers</h1>
              <p className="text-base font-medium">believe in our service</p>
            </div>
            <div>
              <img src={icon2} alt="icon2" />
              <h1 className="text-2xl font-bold">5k+ properties</h1>
              <p className="text-base font-medium">
                and house ready for occupancy
              </p>
            </div>
          </div>
        </div>
        <div className="image-container w-1/2">
          <img
            src={house}
            alt="House"
            className="w-full h-full object-cover" // Added styles
          />
        </div>
      </div>
      {/* search page  */}
      <div className="search relative">
        <img
          src={Search}
          alt="Search"
          className="w-full h-[600px] object-cover "
        />
        <div className="absolute inset-0 bg-gradient-to-t from-[#323232] to-transparent" />
        <h1 className="absolute top-1/3 left-1/2 transform -translate-x-1/2 -translate-y-1/2 font-bold text-center text-5xl text-white">
          Search it . Explore it . Buy it
        </h1>
        <div className="absolute bottom-20 left-1/2 transform -translate-x-[25%]  -translate-y-[100px] w-2/3 flex items-center justify-center">
          <div className="relative w-full">
            {" "}
            {/* Wrapper for search input and button */}
            <input
              type="text"
              placeholder="Search for your desired property "
              value={searchTerm}
              onChange={handleSearchChange}
              className="bg-transparent  border border-white   placeholder-[#000929] bg-white focus:outline-none px-4 py-6 rounded-lg w-1/2 "
            />
            <button className="absolute top-1/4 right-3 transform -translate-x-[640px] -translate-y-[6px] bg-[#7065F0]  text-white font-medium px-6 py-3 w-[10%] rounded-lg">
              Search
            </button>
          </div>
        </div>
      </div>

      {/* search Dedtails  */}
      <div class="search-details bg-[#F0EFFB] px-20 py-5" id="search-details">
        {/* heading  */}
        <div className="justify-center text-center">
          <h1 className="text-4xl font-bold pt-16 pb-6">
            Based on your location
          </h1>
          <p className="font-normal text-base pb-20">
            Some of our picked properties near you location.
          </p>
        </div>

        {/* nav  */}
        <div class="flex flex-col md:flex-row justify-center items-center gap-4 ">
          <div class="flex rounded-lg overflow-hidden bg-[#e6e6ee] p-2 gap-6 justify-start">
            {/* buy and sell button  */}
            <button
              onClick={() => setIsBuy(true)}
              className={`hover:bg-purple-200 text-${
                !isBuy ? "purple-700" : "white"
              }  font-medium py-3 px-9 rounded flex gap-2 ${
                isBuy ? "bg-purple-500 hover:bg-purple-600" : ""
              }`} 
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="h-5 w-5"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                stroke-width="2"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
                />
              </svg>
              Buy
            </button>
            <button
              onClick={() => setIsBuy(false)}
              className={`hover:bg-purple-200 text-${
                isBuy ? "purple-700" : "white"
              }  font-medium py-3 px-9 rounded flex gap-2 ${
                !isBuy ? "bg-purple-500 hover:bg-purple-600" : ""
              }`} 
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="h-5 w-5"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path d="M9 2a1 1 0 000 2h2a1 1 0 100-2H9z" />
                <path
                  fill-rule="evenodd"
                  d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm3 4a1 1 0 000 2h.01a1 1 0 100-2H7zm3 0a1 1 0 000 2h3a1 1 0 100-2h-3zm-3 4a1 1 0 100 2h.01a1 1 0 100-2H7zm3 0a1 1 0 100 2h3a1 1 0 100-2h-3z"
                  clip-rule="evenodd"
                />
              </svg>
              Sell
            </button>
          </div>
          {/* search bar  */}
          <div class="w-1/2 flex justify-end items-center ">
            <div class="relative w-[500px]">
              {" "}
              <input
                type="text"
                placeholder="Search..."
                class="pl-10 pr-4 py-3 w-full rounded-full bg-white border border-gray-300 focus:outline-none focus:ring-2 focus:ring-[#4361EE] focus:border-transparent"
                value={searchTerm}
                onChange={handleSearchChange}
              />
              <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="h-5 w-5 text-[#4361EE]"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                  />
                </svg>
              </div>
            </div>
          </div>
        </div>

        {/* Content based on Buy/Sell */}
        {isBuy ? (
          // Display cards when isBuy is true
          // cards
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-24 py-24">
            {/* Card 1 */}
          <div
            className={`card p-10 border bg-[white] rounded-md shadow-md ${
              expandedCardIndex === 0 ? "transform scale-125" : ""
            }`}
            onClick={() => handleCardClick(0)}
          >
            <img
              src={house1}
              alt="house1"
              className="rounded-md mb-4 w-full"
            />
            <div className="flex justify-between items-center">
              <div className="flex gap-2 items-center text-violet-600 font-semibold">
                <img src={ether} alt="ether" className="h-10 w-10" />
                <p className="font-bold text-4xl">2 ETH</p>
              </div>
              <button className="px-9 py-2 bg-violet-500 text-white font-medium text-lg rounded-md hover:bg-violet-600">
                For Sale
              </button>
            </div>
            <h1 className="text-3xl font-bold text-violet-600 py-6">
              Norbu House
            </h1>
            <div className="flex items-start gap-4 text-gray-600 mt-2">
              <img src={location} alt="location" className="h-5 w-4" />
              <p className="font-bold text-sm">Motithang,Thimphu</p>
            </div>
            <div className="flex items-start gap-4 text-gray-600 mt-2">
              <img src={phone} alt="phone" className="h-4 w-4" />
              <p className="font-bold text-sm">+975 17495130</p>
            </div>
            <hr className="border-[#e0e0e6] my-4" />
            <div className="flex justify-between items-center text-gray-600">
              <div className="flex items-center gap-2">
                <img src={bed} alt="bed" className="h-4 w-5" />
                <p className="text-xl">4 Beds</p>
              </div>
              <div className="flex items-center gap-2">
                <img src={bath} alt="bath" className="h-4 w-5" />
                <p className="text-xl">3 Bathrooms</p>
              </div>
              <div className="flex items-center gap-2">
                <img src={land} alt="land" className="h-5 w-5" />
                <p className="text-xl">15 Dec</p>
              </div>
            </div>
            {/* Show Buy and Cancel buttons when expanded */}
            {expandedCardIndex === 0 && (
              <div className="mt-6">
                <button className="px-9 py-2 bg-green-500 text-white font-medium text-lg rounded-md hover:bg-green-600 mr-4">
                  Buy
                </button>
                <button
                  className="px-9 py-2 bg-red-500 text-white font-medium text-lg rounded-md hover:bg-red-600"
                  onClick={() => handleCardClick(0)}
                >
                  Cancel
                </button>
              </div>
            )}
          </div>

          {/* Card 2 */}
          
          {/* Card 3 */}
          
        </div>
        ) : (
          // selling form 
          // Display sell form when isBuy is false
          <div className="bg-gray-100 rounded-lg shadow-md p-8 mx-auto mt-12 max-w-screen-md">
          <h2 className="text-4xl font-bold text-center text-violet-600 mb-2">
            Property listing
          </h2>
          <p className="text-center text-gray-600 mb-10">
            List Your Properties Seamlessly on Our DApp for Global Reach and Transparency!
          </p>
          <form className="w-full">
            <div className="flex flex-col space-y-4">
              {/* Property Name and ID */}
              <div className="flex space-x-4">
                <div className="w-1/2">
                  <label htmlFor="propertyName" className="block text-sm font-medium text-gray-700">
                    Property name
                  </label>
                  <input 
                    type="text" 
                    id="propertyName" 
                    className="mt-1 block w-full rounded-md shadow-sm focus:border-[#7065F0] focus:ring-[#7065F0] sm:text-sm px-4 py-4" 
                    placeholder="Enter property name" 
                  />
                </div>
                <div className="w-1/2">
                  <label htmlFor="propertyId" className="block text-sm font-medium text-gray-700">
                    Property ID
                  </label>
                  <input 
                    type="text" 
                    id="propertyId" 
                    className="mt-1 block w-full rounded-md shadow-sm focus:border-[#7065F0] focus:ring-[#7065F0] sm:text-sm px-4 py-4" 
                    placeholder="Enter property ID" 
                  />
                </div>
              </div>
    
              {/* Property Description */}
              <div>
                <label htmlFor="propertyDescription" className="block text-sm font-medium text-gray-700">
                  Property description
                </label>
                <textarea 
                  id="propertyDescription" 
                  rows="4" 
                  className="mt-1 block w-full rounded-md shadow-sm focus:border-[#7065F0] focus:ring-[#7065F0] sm:text-sm px-4 py-4" 
                  placeholder="Enter property description"
                ></textarea>
              </div>
    
              {/* Bedrooms, Bathroom, Land Size, Property Address */}
              <div className="flex flex-wrap space-x-4">
                {['Bedrooms', 'Bathroom', 'Land size', 'Property address'].map((field) => (
                  <div key={field} className="w-1/2 md:w-1/4 mb-4 ">
                    <label htmlFor={field.toLowerCase().replace(' ', '')} className="block text-sm font-medium text-gray-700">
                      {field}
                    </label>
                    <input 
                      type="text" 
                      id={field.toLowerCase().replace(' ', '')} 
                      className="mt-1 block w-full rounded-md shadow-sm focus:border-[#7065F0] focus:ring-[#7065F0] sm:text-sm px-4 py-4 "
                      placeholder={`Enter ${field}`} 
                    />
                  </div>
                ))}
              </div>
    
              {/* Price and Established Year */}
              <div className="flex space-x-4">
                <div className="w-1/2">
                  <label htmlFor="price" className="block text-sm font-medium text-gray-700">
                    Price
                  </label>
                  <input 
                    type="number" 
                    id="price" 
                    className="mt-1 block w-full rounded-md shadow-sm focus:border-[#7065F0] focus:ring-[#7065F0] sm:text-sm px-4 py-4 " 
                    placeholder="Enter price in ETH" 
                  />
                </div>
                <div className="w-1/2">
                  <label htmlFor="establishedYear" className="block text-sm font-medium text-gray-700">
                    Established Year
                  </label>
                  <input 
                    type="number" 
                    id="establishedYear" 
                    className="mt-1 block w-full rounded-md shadow-sm focus:border-[#7065F0] focus:ring-[#7065F0] sm:text-sm px-4 py-4 " 
                    placeholder="Enter year" 
                  />
                </div>
              </div>
    
              {/* Property Pictures and Contact Info */}
              <div className="flex space-x-4">
                <div className="w-1/2">
                  <label htmlFor="propertyPictures" className="block text-sm font-medium text-gray-700">
                    Property pictures
                  </label>
                  <div className="mt-1 flex rounded-md shadow-sm">
                    <input 
                      type="file" 
                      id="propertyPictures" 
                      className="sr-only" 
                      multiple
                    />
                    <button type="button" className="inline-flex items-center px-4 py-2 border border-gray-300 rounded-l-md text-sm font-medium text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-violet-500">
                      Select folder
                    </button>
                  </div>
                </div>
                <div className="w-1/2">
                  <label htmlFor="contactInfo" className="block text-sm font-medium text-gray-700">
                    Contact Info
                  </label>
                  <input 
                    type="text" 
                    id="contactInfo" 
                    className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-violet-500 focus:ring-violet-500 sm:text-sm px-4 py-4" 
                    placeholder="Enter contact info" 
                  />
                </div>
              </div>
            </div>
    
            {/* Mint Property Button */}
            <div className="mt-6 flex justify-center">
              <button 
                type="submit"
                className="px-6 py-3 rounded-md text-white bg-violet-500 hover:bg-violet-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-violet-500"
              >
                Mint Property
              </button>
            </div>
          </form>
        </div>
        )}
      </div>

      {/* about us page  */}
      <div class="AboutUs  min-h-screen flex flex-col items-center justify-center" id="AboutUs">
        <h1 class="text-4xl font-bold  mb-8 pt-14">About Us</h1>
        <div class="flex flex-col lg:flex-row items-center justify-center gap-16">
          <div class="w-full lg:w-1/2 p-40">
            <h2 class="text-5xl font-bold mb-4 leading-relaxed">
              We make it easy for <br />{" "}
              <span class="text-[#7065F0]">customers</span> and{" "}
              <span class="text-[#7065F0]">landlords</span>
            </h2>
            <h3 class="text-2xl font-bold  mb-2">
              The new way to find your new home
            </h3>
            <p class="mb-6 text-xl  ">
              We merge the timeless reliability of real estate with the <br />
              cutting-edge innovation of blockchain technology.
            </p>

            <h3 class="text-2xl font-bold mb-4">Our mission</h3>
            <p class=" mb-6 font-medium text-xl ">
              To revolutionize property transactions by enhancing:
            </p>

            <div class="grid grid-cols-1 md:grid-cols-3 gap-4 mb-8">
              <div class="flex items-center">
                <img src={security} alt="" class="w-8 h-8 mr-4" />
                <p class="font-medium text-xl ">Security</p>
              </div>
            </div>

            <div class="grid grid-cols-1 md:grid-cols-3 gap-4 mb-8">
              <div class="flex items-center">
                <img src={transparency} alt="" class="w-8 h-8 mr-4" />
                <p class="font-medium text-xl ">Transparency</p>
              </div>
            </div>

            <div class="grid grid-cols-1 md:grid-cols-3 gap-4 mb-8">
              <div class="flex items-center">
                <img src={efficiency} alt="" class="w-8 h-8 mr-4" />
                <p class="font-medium text-xl ">Efficiency</p>
              </div>
            </div>

            <p class=" font-medium text-xl ">
              With expertise in both real estate and blockchain, we're <br />{" "}
              committed to shaping the future of property ownership and
              management. Join us on this transformative journey.
            </p>
          </div>

          <div class="w-full lg:w-1/2">
            <img src={hd} alt="hd" class="w-full h-auto" />
          </div>
        </div>
      </div>

      <div class="aim container mx-auto  py-16 " id="aim">
        <h1 class="text-4xl font-bold text-center mb-12 leading-relaxed ">
          Why Choose Our Properties <br /> Of Real Estate Industries
        </h1>

        <div class="grid grid-cols-2 gap-2">
          <div class=" rounded-lg  flex flex-col justify-center items-center">
            <img
              src={illustration}
              alt="illustration"
              class="w-100% h-100% mb-6"
            />
          </div>

          <div class="grid grid-cols-2 gap-4">
            <div class="bg-white rounded-lg p-6 flex flex-col justify-start items-start">
              <img src={Hcheck} alt="Hcheck" class="w-100% h-100% mb-4 " />
              <h3 class="text-2xl font-bold mb-2">Property Insurance</h3>
              <p>
                We offer our customer property protection of liability coverage
                and insurance for their better life.
              </p>
            </div>

            <div class="bg-white rounded-lg p-6 flex flex-col justify-start items-start">
              <img src={dollar} alt="dollar" class="w-100% h-100% mb-4" />
              <h3 class="text-2xl font-bold mb-2">Best Price</h3>
              <p>
                Not sure what you should be charging for your property? No need
                to worry, let us do the numbers for you.
              </p>
            </div>

            <div class="bg-white rounded-lg p-6 flex flex-col justify-start items-start">
              <img src={down} alt="down" class="w-100% h-100% mb-4" />
              <h3 class="text-2xl font-bold mb-2">Lowest Commission</h3>
              <p>
                You no longer have to negotiate commissions and haggle with
                other agents it only cost 2%!
              </p>
            </div>

            <div class="bg-white rounded-lg p-6 flex flex-col justify-start items-start">
              <img src={loco} alt="loco" class="w-100% h-100% mb-4" />
              <h3 class="text-2xl font-bold mb-2">Overall Control</h3>
              <p>
                Get a virtual tour, and schedule visits before you rent or buy
                any properties. You get overall control.
              </p>
            </div>
          </div>
        </div>
      </div>

      <div className=" footer bg-white py-16">
        <div className="container mx-auto px-4">
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-10">
            {/* Left Column - Navigation & Feedback */}
            <div>
              <div className="flex space-x-6">
                <Link to="/" className="text-gray-800 hover:text-[#7065F0]">
                  Home
                </Link>
                <Link to="/" className="text-gray-800 hover:text-[#7065F0]">
                  Buy
                </Link>
                <Link to="/" className="text-gray-800 hover:text-[#7065F0]">
                  Sell
                </Link>
                <Link to="/" className="text-gray-800 hover:text-[#7065F0]">
                  Services
                </Link>
                <Link to="/" className="text-gray-800 hover:text-[#7065F0]">
                  About Us
                </Link>
              </div>

              <div className="mt-8">
                <p className="font-semibold text-gray-800 mb-6">
                  Have any feedback regarding our website?
                </p>
                <div className="mt-2 flex rounded-lg shadow-sm">
                  <input
                    type="text"
                    className="w-full rounded-l-lg px-4 py-2 border focus:ring-[#7065F0] focus:border-[#7065F0]"
                    placeholder="Enter..."
                  />
                  <button className="px-4 py-2 rounded-r-lg bg-[#7065F0] text-white font-medium hover:bg-[#7065F0] focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-[#7065F0]">
                    Send
                  </button>
                </div>
              </div>
            </div>

            {/* Middle Column - Logo & Description */}
            <div className="  flex flex-col items-center">
              <img src={logo} alt="logo" className="h-12 w-auto" />
              <div className="mt-4 text-center">
                <h2 className="text-lg font-semibold text-gray-800">
                  Keychain
                </h2>
                <p className="text-gray-600">Blockchain Real Estate</p>
                <p className="text-gray-600">Find Your Dream House</p>
              </div>
            </div>

            {/* Right Column - Contact & Social Media */}
            <div>
              <div className="flex items-center space-x-4">
                <img src={phone} alt="phone" className="h-6 w-auto" />
                <p className="text-gray-800">+975 17483980</p>
              </div>

              <div className="mt-8 flex space-x-4">
                <Link
                  to="/"
                  className="px-4 py-2 border  rounded text-gray-800 hover:bg-[#7065F0] hover:text-[white]"
                >
                  Facebook
                </Link>
                <Link
                  to="/"
                  className="px-4 py-2 border  rounded- text-gray-800 hover:bg-[#7065F0] hover:text-[white]"
                >
                  Instagram
                </Link>
                <Link
                  to="/"
                  className="px-4 py-2 border rounded  text-gray-800 hover:bg-[#7065F0] hover:text-[white]"
                >
                  LinkedIn
                </Link>
                <Link
                  to="/"
                  className="px-4 py-2 border  rounded  text-gray-800 hover:bg-[#7065F0] hover:text-[white]"
                >
                  Twitter
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Public;
